namespace ASCOM.Meade.net
{
    public class ParkedPosition
    {
        public double Altitude { get; set; }
        public double Azimuth { get; set; }
        public double RightAscension { get; set; }
        public double Declination { get; set; }
    }
}